package ControlStatements;

public class WhileExercitiu1 {
    public static void main(String[] args) {
        System.out.println("Program care imi calculeaza cate cadouri " +
                "pot sa cumpar de craciun!");
        int pretCadou = 28;
        int sumaMaximaDisponibila = 236;
        int numarulDeCadouriPecareIlPotCumpara = 0;

        while(sumaMaximaDisponibila >= 0) {
            numarulDeCadouriPecareIlPotCumpara++;
            sumaMaximaDisponibila = sumaMaximaDisponibila - pretCadou;
        }
        System.out.println("Anul acesta pot cumpara" + " " +numarulDeCadouriPecareIlPotCumpara + "cadouri");

    }
}
